import React from 'react';
import {
  Button,
} from 'reactstrap';

const PadButton = ({ value, setData }) =>  {
  const updatePad = (event) => {
    event.preventDefault();
    setData(data => ({ ...data, pedido: data.pedido.toString() + value }));
  };

  return (
    <Button
      block
      color="secondary"
      size="lg"
      className="p-3"
      onClick={ updatePad }
    >
      { value }
    </Button>
  );
}

export default PadButton;