import React, { useState } from 'react';
import {
  Button,
  Card,
  CardBody,
  CardHeader,
  Col,
  Form,
  Input,
  Row,
} from 'reactstrap';

import PadButton from './PadButton';

const PainelChamada = ({ onChamada }) =>  {
  const [data, setData] = useState({ pedido: "", pedidosAnteriores: [] });

  const doSubmit = (event) => {
    event.preventDefault();
    onChamada(data.pedido);
    gravarPedidoAnterior(data.pedido);
    zerarCampoPedido();
  };
  
  const doChamada = pedido => event => {
    event.preventDefault();
    onChamada(pedido);
  };

  const doBackspace = (event) => {
    event.preventDefault();
    setData(data => ({ ...data, pedido: data.pedido.slice(0, -1) }));
  };

  const zerarCampoPedido = () => {
    setData(data => ({ ...data, pedido: "" }));
  };
  
  const gravarPedidoAnterior = pedido => {
    setData(data => {
      if (data.pedidosAnteriores.includes(pedido)) {
        return;
      }
      
      const pedidos = data.pedidosAnteriores;
      pedidos.unshift(pedido);
      
      return { ...data, pedidosAnteriores: pedidos.slice(0, 4) };
    });
  }

  return (
    <Card>
      <CardHeader>
        <strong>Pedido</strong>
      </CardHeader>
      <CardBody>
        <Form onSubmit={ doSubmit }>
          <Row>
            <Col xs="12">
              <Input
                  id="name"
                  type="number"
                  size="lg"
                  className="text-center"
                  placeholder="999"
                  onChange={e => {const value = e.target.value; setData(data => ({...data, pedido: value}));}}
                  value={data.pedido}
              />
            </Col>
          </Row>
          <Row className="mt-2">
            {
              data.pedidosAnteriores.map(pedido => (
                <Col xs="3" id={ pedido } >
                  <Button
                    outline
                    block
                    size="lg"
                    color="secondary"
                    className="p-3"
                    onClick={ doChamada(pedido) }
                  >
                    { pedido }
                  </Button>
                </Col>
              ))
            }
            {
              (new Array(4 - data.pedidosAnteriores.length)).fill("").map(() => (
                <Col xs="3">
                  <Button
                    outline
                    block
                    size="lg"
                    color="secondary"
                    className="p-3"
                    onClick={ e => e.preventDefault() }
                  >
                    &nbsp;
                  </Button>
                </Col>
              ))
            }
          </Row>
          <Row className="mt-2">
            <Col xs="4">
              <PadButton value="1" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="2" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="3" setData={ setData } />
            </Col>
          </Row>
          <Row className="mt-2">
            <Col xs="4">
              <PadButton value="4" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="5" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="6" setData={ setData } />
            </Col>
          </Row>
          <Row className="mt-2">
            <Col xs="4">
              <PadButton value="7" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="8" setData={ setData } />
            </Col>
            <Col xs="4">
              <PadButton value="9" setData={ setData } />
            </Col>
          </Row>
          <Row className="mt-2">
            <Col xs="4">
              <Button
                block
                color="secondary"
                size="lg"
                className="p-3"
                onClick={ doBackspace }
              >
                <i class="cui-delete" title="Chamar"></i>
              </Button>
            </Col>
            <Col xs="4">
              <PadButton value="0" setData={ setData } />
            </Col>
            <Col xs="4">
              <Button
                type="submit"
                color="primary"
                size="lg"
                className="p-3"
                block
              >
                <i class="icon-paper-plane" title="Chamar"></i>
              </Button>
            </Col>
          </Row>
        </Form>
      </CardBody>
    </Card>
  );
}

export default PainelChamada;